<?php

/**
 * @file
 * uw_ofis_eng.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ofis_eng_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_field_default_field_instances_alter().
 */
function uw_ofis_eng_field_default_field_instances_alter(&$data) {
  if (isset($data['node-uw_ct_single_page_home-field_sph_blocks'])) {
    $data['node-uw_ct_single_page_home-field_sph_blocks']['description'] = '<b>Captioned image block:</b> An image block that will align left or right with captioned text, image text and icons.<br>
    <b>Copy block:</b> A free-formed text area to display embeds and widgets, or content that will not fit within existing Block types.<br>
    <b>Events block:</b> A block to display both wcms and non-wcms events.<br>
    <b>Heading block:</b> Display a title, icon and fact/figure with an optional image.<br>
    <b>Image block:</b> An image that will be the entire width of the page.<br>
    <b>Marketing block:</b> To display background images overlaid with quotes.<br>
    <b>Navigation block:</b> Display an text and/or icon to defined anchor links within the page.<br>
    <b>Options block:</b> A linked image to direct users to a specific piece of content or web site.<br>
    <b>Quicklinks block:</b> Display an icon and title with multiple quicklinks.<br>
    <b>Stories block:</b> Display a title and content in the form of a story.'; /* WAS: '<b>Captioned image block:</b> An image block that will align left or right with captioned text, image text and icons.<br>
    <b>Copy block:</b> A free-formed text area to display embeds and widgets, or content that will not fit within existing Block types.<br>
    <b>Events block:</b> A block to display both wcms and non-wcms events.<br>
    <b>Heading block:</b> Display a title, icon and fact/figure with an optional image.<br>
    <b>Image block:</b> An image that will be the entire width of the page.<br>
    <b>Marketing block:</b> To display background images overlaid with quotes.<br>
    <b>Navigation block:</b> Display an text and/or icon to defined anchor links within the page.<br>
    <b>Options block:</b> A linked image to direct users to a specific piece of content or web site.<br>
    <b>Quicklinks block:</b> Display an icon and title with multiple quicklinks.<br>
    <b>Stories block:</b> Display a title and content in the form of a story.' */
    $data['node-uw_ct_single_page_home-field_sph_blocks']['settings']['allowed_bundles']['ofis_block'] = 'ofis_block'; /* WAS: '' */
    $data['node-uw_ct_single_page_home-field_sph_blocks']['settings']['bundle_weights']['ofis_block'] = 31; /* WAS: '' */
  }
}

/**
 * Implements hook_paragraphs_info().
 */
function uw_ofis_eng_paragraphs_info() {
  $items = array(
    'ofis_block' => array(
      'name' => 'OFIS block',
      'bundle' => 'ofis_block',
      'locked' => '1',
    ),
  );
  return $items;
}
