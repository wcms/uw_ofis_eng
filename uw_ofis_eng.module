<?php

/**
 * @file
 * OFIS Eng file.
 */

require_once 'uw_ofis_eng.features.inc';

/**
 * @file
 * Queries OFIS Engineering people.
 */

/**
 * Implements hook_menu().
 */
function uw_ofis_eng_menu() {

  $items = array();

  $items['ofis/search/%'] = [
    'title' => 'OFIS Results',
    'page callback' => 'uw_ofis_eng_get_matches',
    'page arguments' => array(2),
    // No access checks.
    'access callback' => TRUE,
    'delivery callback' => 'uw_ofis_eng_deliver',
    'type' => MENU_CALLBACK,
  ];

  $items['admin/config/system/ofis-eng'] = [
    'title' => 'OFIS Research Expert Search settings',
    'description' => 'Configure OFIS Research Expert Search',
    'page callback' => 'drupal_get_form',
    'access arguments' => ['administer uw_ofis_eng settings'],
    'page arguments' => ['uw_ofis_eng_admin_form'],
    'file' => 'includes/uw_ofis_eng.admin.inc',
  ];

  return $items;
}

/**
 * Implements hook_permission().
 */
function uw_ofis_eng_permission() {
  return array(
    'administer uw_ofis_eng settings' => array(
      'title' => t('administer OFIS Research Expert Search'),
    ),
  );
}

/**
 * Implements hook_block_info().
 */
function uw_ofis_eng_block_info() {
  $blocks = array();

  $blocks['uw_ofis_eng_search_block'] = array(
    'info' => t('OFIS Research Expert Search Block'),
    'cache' => DRUPAL_NO_CACHE,
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function uw_ofis_eng_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'uw_ofis_eng_search_block':
      $block['subject'] = t("Find a Research Expert");
      $block['content'] = uw_ofis_eng_search_block_view();
      break;
  }

  return $block;
}

/**
 * Implements hook_theme().
 */
function uw_ofis_eng_theme($existing, $type, $theme, $path) {
  return [
    'block__uw_ofis_eng' => [
      'original hook' => 'block',
      'template' => 'block--ofis',
      'path' => drupal_get_path('module', 'uw_ofis_eng') . '/templates',
    ],
  ];
}

/**
 * OFIS block.
 *
 * Custom function to assemble renderable array for block content.
 * Returns a renderable array with the block content.
 *
 * @return array
 *   returns a renderable array of block content.
 */
function uw_ofis_eng_search_block_view() {

  // Block output in HTML with div wrapper.
  $block = array(
    '#markup' => "<input placeholder=\"Search for keyword, name, or research area\" name=\"keyword\"
            autocomplete=\"off\" type=\"text\" id=\"ofis-search\" />
            <input type=\"button\" value=\"Search\" id=\"ofis-submit\"/>
              <div id='ofis-results'>
                <div  class=\"suggestionList\" id=\"autoSuggestionsList\">
                  Start searching to view results.
                </div>
                <div class=\"waiting\"><i class=\"fa fa-spinner\" aria-hidden=\"true\"></i></div>
              </div>",
    '#attached' => array(
      'css' => array(
        drupal_get_path('module', 'uw_ofis_eng') . '/css/style.css',
      ),
      'js' => array(
        drupal_get_path('module', 'uw_ofis_eng') . '/js/script.js',
      ),
    ),
  );

  return $block;
}

/**
 * Single person render array.
 *
 * @param object $person
 *   Person as object.
 *
 * @return false|string
 *   Array if successful, empty string otherwise.
 */
function uw_ofis_eng_get_person($person) {
  $template_file = drupal_get_path('module', 'uw_ofis_eng') . '/templates/person.tpl.php';
  $variables = array();
  $variables['first_name'] = $person->first_name;
  $variables['last_name'] = $person->last_name;
  $variables['departments'] = $person->departments;
  $variables['email'] = $person->email;
  $variables['phone'] = $person->phone;
  $variables['research_interests'] = $person->keywords;
  $variables['user_id'] = $person->userid;
  $variables['department_id'] = uw_ofis_eng_get_department_id($person->departments[0]);

  if ($person) {
    return theme_render_template($template_file, $variables);
  }

  return '';
}

/**
 * API match using multiple keywords.
 *
 * @param string $keyword
 *   One or more query keywords.
 *
 * @return array|mixed|string|null
 *   Results as array if found, or "no result" string.
 */
function uw_ofis_eng_get_matches($keyword) {
  $dep = variable_get('uw_ofis_eng_department_id', 'eng');
  $url = sprintf('https://ofis.uwaterloo.ca/api/index.php/list/faculty/%s/%s', $dep, rawurlencode($keyword));
  $results = uw_ofis_eng_fetch_data($url);
  $experts = array();

  if ($results) {
    foreach ($results as $expert) {
      $experts[] = uw_ofis_eng_get_person($expert);
    }
    return "<p>" . t("Number of experts found:") . " " . count($experts) . "</p>" . implode("", $experts);
  }
  elseif (strpos($keyword, " ") !== FALSE) {
    // Possibly searching for first name + last name.
    $keywords = explode(" ", $keyword);

    $experts = [];
    foreach ($keywords as $name) {
      $experts[] = uw_ofis_eng_get_match($name);
    }

    $experts = array_merge([], ...$experts);

    return "<p>" . t("Number of experts found:") . " " . count($experts) . "</p>" . implode("", $experts);

  }
  else {
    return t('No results, please try again.');
  }
}

/**
 * Mapping results from API for a single keyword.
 *
 * @param string $keyword
 *   Single query keyword.
 *
 * @return array
 *   Mapping users, using key/value pairs, where key is user id, and
 *   value is render array of the person.
 */
function uw_ofis_eng_get_match($keyword) {
  $dep = variable_get('uw_ofis_eng_department_id', 'eng');
  $url = sprintf('https://ofis.uwaterloo.ca/api/index.php/list/faculty/%s/%s', $dep, rawurlencode($keyword));
  $results = uw_ofis_eng_fetch_data($url);
  $experts = array();

  if ($results) {
    foreach ($results as $expert) {
      $experts[$expert->userid] = uw_ofis_eng_get_person($expert);
    }
  }

  return $experts;
}

/**
 * Renderer.
 *
 * @param mixed|string|array $page_callback_result
 *   Result element.
 */
function uw_ofis_eng_deliver($page_callback_result) {
  print render($page_callback_result);
}

/**
 * OFIS API request.
 *
 * @param string $url
 *   API endpoint.
 *
 * @return bool|array
 *   For successful request array will be returned. Otherwise, FALSE.
 *
 * @throws \JsonException
 */
function uw_ofis_eng_fetch_data($url) {
  $options = array(
    'method'  => 'GET',
    'timeout' => 15,
  );
  $response = drupal_http_request($url, $options);
  if ((int) $response->code !== 200) {
    return FALSE;
  }
  $data = $response->data;

  return json_decode($data, FALSE, 512, JSON_THROW_ON_ERROR);
}

/**
 * Returns department name as id.
 *
 * @param string $department_name
 *   Department name.
 *
 * @return string
 *   Department id.
 */
function uw_ofis_eng_get_department_id($department_name) {
  $department_id = "management-sciences";

  $departments = [
    // Engineering departments.
    'Department of Chemical Engineering' => 'chemical-engineering',
    'Department of Civil and Environmental Engineering' => 'civil-environmental-engineering',
    'Department of Electrical and Computer Engineering' => 'electrical-computer-engineering',
    'Department of Environmental Engineering' => 'civil-environmental-engineering',
    'Department of Geological Engineering' => 'civil-environmental-engineering',
    'Department of Management Sciences and Engineering' => 'management-sciences',
    'Department of Mechanical and Mechatronics Engineering' => 'mechanical-mechatronics-engineering',
    'Department of Systems Design Engineering' => 'systems-design-engineering',
    'School of Architecture' => 'architecture',
    // Science departments.
    'Department of Biology' => 'biology',
    'Department of Chemistry' => 'chemistry',
    'Department of Earth and Environmental Sciences' => 'earth-environmental-sciences',
    'Department of Physics and Astronomy' => 'physics-astronomy',
    'School of Optometry and Vision Science' => 'optometry-vision-science',
    'School of Pharmacy' => 'pharmacy',
  ];

  if (isset($departments[$department_name])) {
    $department_id = $departments[$department_name];
  }

  return $department_id;
}

/**
 * Implements hook_navbar().
 */
function uw_ofis_eng_navbar() {
  $items['uw_ofis_eng'] = array(
    '#attached' => array(
      'css' => array(drupal_get_path('module', 'uw_ofis_eng') . '/uw_ofis_eng.navbar.icons.css'),
    ),
  );
  return $items;
}
