<?php

/**
 * @file
 * Person template file.
 */
?>
<div class="expert">
  <h2><?php print $first_name ?>  <?php print $last_name; ?> <button class="expand" aria-expanded="false"><i class="fa fa-plus"></i></button></h2>
  <div class="profile">
    <?php
    foreach ($departments as $department) { ?>
      <div class="department"><?php print $department; ?></div>
    <?php }; ?>
    <div class="contact">Email: <?php print $email; ?><?php if ($phone) {
   ?> | Phone: <?php print $phone; ?> <?php
   } ?></div>
  </div>
  <div class="research-interests hidden">
    <?php if ($research_interests) { ?>
      <p><strong>Research Interests</strong></p>
      <ul>
        <?php
        foreach ($research_interests as $interest) { ?>
          <li><?php print $interest; ?></li>

        <?php }; ?>
      </ul>
    <?php } ?>
    <div class="more">
      <a href="https://uwaterloo.ca/<?php print $department_id; ?>/profile/<?php print $user_id ?>" target="_blank" class="button">Show full profile</a>
    </div>
  </div>
</div>
