<?php

/**
 * @file
 * uw_ofis_eng.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ofis_eng_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management_ofis-research-expert-search-settings:admin/config/system/ofis-eng.
  $menu_links['management_ofis-research-expert-search-settings:admin/config/system/ofis-eng'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/config/system/ofis-eng',
    'router_path' => 'admin/config/system',
    'link_title' => 'OFIS Research Expert Search settings',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'management_ofis-research-expert-search-settings:admin/config/system/ofis-eng',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -11,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
    'parent_identifier' => 'management_administration:admin',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('OFIS Research Expert Search settings');

  return $menu_links;
}
