<?php

/**
 * @file
 * OFIS Eng admin file.
 */

/**
 * OFIS Eng admin form.
 *
 * @param array $form
 *   Form element.
 * @param array|mixed $form_state
 *   Form state element.
 *
 * @return array
 *   Form render array.
 */
function uw_ofis_eng_admin_form(array $form, &$form_state) {

  $form['wrapper'] = [
    '#type' => 'fieldset',
    '#title' => t('OFIS Research Expert Search configuration'),
  ];

  $form['wrapper']['uw_ofis_eng_department_id'] = [
    '#type' => 'textfield',
    '#size' => 10,
    '#title' => t('Departmental search'),
    '#default_value' => variable_get('uw_ofis_eng_department_id', 'eng'),
    '#description' => t('Allowing search for specific departments. Example "eng", or "sci".'),
  ];

  return system_settings_form($form);
}

/**
 * Admin form validation.
 *
 * @param array $form
 *   Render array form.
 * @param array|mixed $form_state
 *   Form with values.
 */
function uw_ofis_eng_admin_form_validate(array $form, &$form_state) {

  // Basic validation, making sure only small caps letters are entered,
  // with length from 3 to 5 characters.
  if (!preg_match('/^[a-z]{3,5}$/', $form_state['values']['uw_ofis_eng_department_id'])) {
    form_set_error('uw_ofis_eng_department_id', t('Enter valid department'));
  }
}
